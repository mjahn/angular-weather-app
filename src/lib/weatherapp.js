(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(['exports', './controller/weathercontroller', './service/weatherservice', './service/translationservice'], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require('./controller/weathercontroller'), require('./service/weatherservice'), require('./service/translationservice'));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.weathercontroller, global.weatherservice, global.translationservice);
    global.app = mod.exports;
  }
})(this, function (exports) {
  (function (global, factory) {
    if (typeof define === "function" && define.amd) {
      define(['./controller/weathercontroller', './service/weatherservice', './service/translationservice'], factory);
    } else if (typeof exports !== "undefined") {
      factory();
    } else {
      var mod = {
        exports: {}
      };
      factory(global.weathercontroller, global.weatherservice, global.translationservice);
      global.app = mod.exports;
    }
  })(this, function (_weathercontroller, _weatherservice, _translationservice) {
    'use strict';

    var _weathercontroller2 = _interopRequireDefault(_weathercontroller);

    var _weatherservice2 = _interopRequireDefault(_weatherservice);

    var _translationservice2 = _interopRequireDefault(_translationservice);

    function _interopRequireDefault(obj) {
      return obj && obj.__esModule ? obj : {
        default: obj
      };
    }

    var app = angular.module('WeatherApp', ['ngCookies']);
    app.controller('WeatherController', _weathercontroller2.default);
    app.service('WeatherService', _weatherservice2.default);
    app.service('TranslationService', _translationservice2.default);

    console.log('AA');
  });
});
