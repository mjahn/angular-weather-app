export default class TranslationService {
    getTranslation($scope, language) {
        var path = 'somepath_' + language + '.json';
        var ssid = 'someid_' + language;

        if (sessionStorage) {
            if (sessionStorage.getItem(ssid)) {
                $scope.translation = JSON.parse(sessionStorage.getItem(ssid));
            } else {
                $resource(path).get(function(data) {
                    $scope.translation = data;
                    sessionStorage.setItem(ssid, JSON.stringify($scope.translation));
                });
            };
        } else {
            $resource(path).get(function (data) {
                $scope.translation = data;
            });
        }
    }
}