import WeatherController from './controller/weathercontroller'
import WeatherService from './service/weatherservice'
import TranslationService from './service/translationservice'

const app = angular.module('WeatherApp', ['ngCookies'])
app.controller('WeatherController', WeatherController)
app.service('WeatherService', WeatherService)
app.service('TranslationService', TranslationService)

console.log('AA')